package demo;

import cf4j.Kernel;
import cf4j.Processor;
import cf4j.model.matrixFactorization.Bmf;
import cf4j.qualityMeasures.MAE;
import cf4j.utils.PrintableQualityMeasure;

public class TestCF4J {

	public static void main(String[] args) {
		userUser();
		//matrixFactorization();
	}

	static void userUser() {
		String filename = "src/main/resources/ml-latest-small/ratings_test.csv";
		double testUsers = 0.20; // 20% of test users
		double testItems = 0.20; // 20% of test items

		Kernel.getInstance().open(filename, testUsers, testItems, ",");

		String [] similarityMetrics = {"COR", "JMSD"};
		int [] numberOfNeighbors = {50, 100, 150, 200, 250, 300, 350, 400};

		PrintableQualityMeasure mae = new cf4j.utils.PrintableQualityMeasure("MAE", numberOfNeighbors, similarityMetrics);

		for (String sm : similarityMetrics) {

			// Compute similarity
			if (sm.equals("COR")) {
				Processor.getInstance().testUsersProcess(new cf4j.knn.userToUser.similarities.MetricCorrelation()); 			
			}
			else if (sm.equals("JMSD")) {
				Processor.getInstance().testUsersProcess(new cf4j.knn.userToUser.similarities.MetricJMSD());
			}

			// For each value of k
			for (int k : numberOfNeighbors) {

				// Find the neighbors
				Processor.getInstance().testUsersProcess(new cf4j.knn.userToUser.neighbors.Neighbors(k));

				// Compute predictions using DFM
				Processor.getInstance().testUsersProcess(new cf4j.knn.userToUser.aggregationApproaches.DeviationFromMean());

				// Compute MAE
				Processor.getInstance().testUsersProcess(new cf4j.qualityMeasures.MAE());

				// Retrieve mae an store it
				mae.putError(k, sm, Kernel.gi().getQualityMeasure("MAE"));
			}
		}

		// Print the results
		mae.print();
	}

	static void matrixFactorization() {
		String filename = "src/main/resources/ml-latest-small/ratings_test.csv";
		double testUsers = 0.20;
		double testItems = 0.20;

		Kernel.getInstance().open(filename, testUsers, testItems, ",");

		Bmf factorizationModel = new Bmf(4, 20, 0.01, 0.1);
		factorizationModel.train();
		double p1 = factorizationModel.getPrediction(1, 1);
		double p2 = factorizationModel.getPrediction(1, 2);
		double p3 = factorizationModel.getPrediction(1, 3);
		double p4 = factorizationModel.getPrediction(1, 4);
		double p5 = factorizationModel.getPrediction(1, 5);
		System.out.println(p1);
		System.out.println(p2);
		System.out.println(p3);
		System.out.println(p4);
		System.out.println(p5);
	}
}
