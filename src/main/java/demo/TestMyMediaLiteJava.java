package demo;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;

import org.mymedialite.data.EntityMapping;
import org.mymedialite.data.IPosOnlyFeedback;
import org.mymedialite.eval.Items;
import org.mymedialite.io.ItemData;
import org.mymedialite.itemrec.BPRMF;

public class TestMyMediaLiteJava {

	 public static final String MODEL_FILE = "src/main/resources/ml-latest-small/mymedialitemodel.dat";
	 public static final String TRAINING_DATA_FILE = "src/main/resources/ml-latest-small/ratings_copy.csv";
	 public static final String TEST_DATA_FILE = "src/main/resources/ml-latest-small/ratings_test.csv";
	  
	  public static void main(String[] args) {
	    long start = Calendar.getInstance().getTimeInMillis();

	    // Load the data
	    EntityMapping user_mapping = new EntityMapping();
	    EntityMapping item_mapping = new EntityMapping();

	    IPosOnlyFeedback training_data = null;
	    try {
	      training_data = ItemData.read(TRAINING_DATA_FILE, user_mapping, item_mapping, false);
	    } catch (Exception e) {
	      e.printStackTrace();
	    }

	    IPosOnlyFeedback test_data = null;
	    try {
	      test_data = ItemData.read(TRAINING_DATA_FILE, user_mapping, item_mapping, false);
	    } catch (Exception e) {
	      e.printStackTrace();
	    }

	    // Set up the recommender
	    BPRMF recommender = new BPRMF();
	    //MostPopular recommender = new MostPopular();
	    System.out.println(recommender);
	    recommender.setFeedback(training_data);
	        
	    File file = new File(MODEL_FILE);
	    try {
	      if(file.exists()) {
	        recommender.loadModel(MODEL_FILE);
	      } else {
	        recommender.train();      
	        recommender.saveModel(MODEL_FILE);
	      }
	    } catch (IOException e) {
	      System.err.println(e.getMessage());
	    }

	    /*
	     * Test incremental update.
	     */
	    //		int internalUserId1 = user_mapping.toInternalID(28221);
	    //		recommender.addUser(internalUserId1);
	    //
	    //		int internalItemId1 = item_mapping.toInternalID(2854);
	    //		recommender.addItem(internalItemId1);  
	    //		recommender.addFeedback(internalUserId1, internalItemId1);
	    //
	    //		recommender.addFeedback(internalUserId1, item_mapping.toInternalID(2855));
	    //		recommender.addFeedback(internalUserId1, item_mapping.toInternalID(2985));
	    //		recommender.addFeedback(internalUserId1, item_mapping.toInternalID(2071));
	    //		recommender.addFeedback(internalUserId1, item_mapping.toInternalID(950));
	    //		recommender.addFeedback(internalUserId1, item_mapping.toInternalID(2982));
	    //		recommender.addFeedback(internalUserId1, item_mapping.toInternalID(2857));
	    //		recommender.addFeedback(internalUserId1, item_mapping.toInternalID(2979));
	    //		recommender.addFeedback(internalUserId1, item_mapping.toInternalID(2316));
	    //		recommender.addFeedback(internalUserId1, item_mapping.toInternalID(2978));


	    // Measure the accuracy on the test data set
	    Collection<Integer> candidate_items = training_data.allItems();  // items that will be taken into account in the evaluation
	    Collection<Integer> test_users      = training_data.allUsers();  // users that will be taken into account in the evaluation
	    try {
	      HashMap<String, Double> results = Items.evaluate(recommender, test_data, training_data, test_users, candidate_items);
	      System.out.println("AUC       " + results.get("AUC"));
	      System.out.println("MAP       " + results.get("MAP"));
	      System.out.println("NDCG      " + results.get("NDCG"));
	      System.out.println("prec@5    " + results.get("prec@5"));
	      System.out.println("prec@10   " + results.get("prec@10"));
	      System.out.println("prec@15   " + results.get("prec@15"));
	      System.out.println("num_users " + results.get("num_users"));
	      System.out.println("num_items " + results.get("num_items"));
	      System.out.println();
	    } catch (Exception e) {
	      e.printStackTrace();
	    }

	    // Make a prediction for a certain user and item.
	    /*
	    String userId = "2";
	    String itemId = "3";
	    double prediction = recommender.predict(user_mapping.toInternalID(userId), item_mapping.toInternalID(itemId));
	    System.out.println("userId: " + userId + " itemId: " + itemId + " prediction: " + prediction); 

	    long end = Calendar.getInstance().getTimeInMillis();
	    System.out.println("Time taken: " + ((end - start) / 1000F) + " seconds");
	    start = end;
	    */
	    String userId = "2";
	    //String itemId = "3";
	    for (int i = 1; i < 1000; i++) {
	    	if (i == 9 || i == 14) continue;
	    	String itemId = Integer.toString(i);

			double prediction = recommender.predict(user_mapping.toInternalID(userId), item_mapping.toInternalID(itemId));
			System.out.println("userId: " + userId + " itemId: " + itemId + " prediction: " + prediction); 

			long end = Calendar.getInstance().getTimeInMillis();
			System.out.println("Time taken: " + ((end - start) / 1000F) + " seconds");
			start = end;
	    }
	    System.out.println("finished!");
	  }  
}
