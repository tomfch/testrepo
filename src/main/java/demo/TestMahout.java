package demo;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.mahout.cf.taste.common.TasteException;
import org.apache.mahout.cf.taste.impl.model.file.FileDataModel;
import org.apache.mahout.cf.taste.impl.recommender.svd.ALSWRFactorizer;
import org.apache.mahout.cf.taste.impl.recommender.svd.SVDRecommender;
import org.apache.mahout.cf.taste.model.DataModel;
import org.apache.mahout.cf.taste.recommender.RecommendedItem;

public class TestMahout {
	public static void main(String[] args) {
		DataModel dataModel = null;
		try {
			dataModel = new FileDataModel(new File("src/main/resources/ml-latest-small/ratings_copy.csv")); // file might need to be trimmed to only two columns.
		} catch (IOException e) {
			e.printStackTrace();
		}
		ALSWRFactorizer factorizer = null;
		try {
			factorizer = new ALSWRFactorizer(dataModel, 50, 0.065, 15);
		} catch (TasteException e) {
			e.printStackTrace();
		}
		SVDRecommender recommender = null;
		try {
			recommender = new SVDRecommender(dataModel, factorizer);
		} catch (TasteException e) {
			e.printStackTrace();
		}
		try {
			List<RecommendedItem> recs = recommender.recommend(3, 5);
		} catch (TasteException e) {
			e.printStackTrace();
		}
	}
}
